from flask import Flask, render_template, request, redirect, send_from_directory, url_for, jsonify, make_response, session, escape
from flask_mysqldb import MySQL
from functools import wraps 
from werkzeug.utils import secure_filename
from pydub import AudioSegment
import shutil
import yaml
import requests
import os
import datetime
import random 
import json
import time
import binascii
from pyfcm import FCMNotification
import firebase_admin
from firebase_admin import messaging 
from firebase_admin import credentials

UPLOAD_FOLDER = 'Files'
SERVER_ADRESSE = 'http://tuleap.odc.tn/api/'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif','wav', 'mp3', 'ogg', 'flac'])
API_KEY_FIREBASE_CLOUD_MESSAGINING = "AAAAPLexNTo:APA91bG0-QYfwpa2OC9sdPYBFpOrnnJumQonBXFDJgcb4_7mTIbZjRXlU7ttrYX5VNFcBXEk28KxhwoeEuTDzQ6TbuBwLyICqBhAfNbOr_-2PpJmcRIfWrV-XKC1t5OyE353pGfBi0LT"
FILE_JSON_CONFIG_GOOGLE = "C:\\xampp\\htdocs\\ProjectApi\\api\\google-services.json"
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config["DEBUG"] = True 
db = yaml.load(open('db.yaml'))
app.config['MYSQL_HOST'] = db['mysql_host']
app.config['MYSQL_USER'] = db['mysql_user']
app.config['MYSQL_PASSWORD'] = db['mysql_password']
app.config['MYSQL_DB'] = db['mysql_db']

BROKER_HOST = '10.68.10.10'
BROKER_PORT = 1883
TOPIC_NAME = 'wav_files'

mysql = MySQL(app)

def auth_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if request.authorization :
            #get the login and the password and verify them with our database
            cur = mysql.connection.cursor()
            cur.execute("SELECT count(*) FROM customer WHERE login LIKE %s AND pwd LIKE %s",[request.authorization.username,request.authorization.password])
            (number_of_rows,)=cur.fetchone()
            cur.close()
            if number_of_rows > 0:
                return f(*args, **kwargs)

        return make_response('Could not verfiy!!', 401, {'WWW-Authenticate' : 'Basic realm="Login Required"'})
    return decorated


@app.route('/restApi/Hablo_v1/Android/alertsbytype/<idCustomer>/<idAlert>')
def getAlertMoodyBaby(idCustomer,idAlert):
    cur=None
    try:
        cur = mysql.connection.cursor()
        cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
        (number_of_rows,)=cur.fetchone()
        if number_of_rows > 0:
            cur.execute("SELECT a.id,title,description,alertDate,seen,c.customer_id FROM alert a,customer_alert c WHERE a.id = c.alert_id AND  a.id = %s AND c.customer_id LIKE %s",[idAlert, idCustomer])
            row_headers=[x[0] for x in cur.description] #this will extract row headers
            rv = cur.fetchall()
            json_data=[]
            for result in rv:
                json_data.append(dict(zip(row_headers,result)))
            return jsonify({"alerts":json_data})
        else:
            return make_response('unknown Client',309)
    except Exception as e:
        return make_response('Error in database Connection',506)
    finally:
        if cur is not None:
            cur.close() 

@app.route('/restApi/Hablo_v1/Android/cryalert/<idCustomer>/<idAlert>')
def getAlertCryListbySrc(idCustomer,idAlert):
    cur = None
    try:
        cur = mysql.connection.cursor()
        cur.execute("SELECT COUNT(*) from alert where id LIKE %s",[idAlert])
        (number_of_rows,)=cur.fetchone()
        if number_of_rows > 0:
            cur.execute("SELECT * FROM `alert_cry` WHERE idAlert=%s",[idAlert])
            row_headers=[x[0] for x in cur.description] #this will extract row headers
            rv = cur.fetchall()
            json_data=[]
            for result in rv:
                json_data.append(dict(zip(row_headers,result)))
            return jsonify({"crys":json_data})
        else:
            return make_response('unknown alert Id',309)
    except Exception as e:
        return make_response('Error in database Connection',506)
    finally:
        if cur is not None:
            cur.close()


@app.route('/restApi/Hablo_v1/IA/pushnotif/<idDevice>', methods=['POST'])
def pushNotification(idDevice):
    if request.method == 'POST' :
        cur=None
        try:
            cur = mysql.connection.cursor()
            
            cur.execute("SELECT COUNT(*) from device where id LIKE %s",[idDevice])
            (number_of_rows_device,)=cur.fetchone()

            cur.execute("SELECT COUNT(*) from cry where fileName LIKE %s",[request.form['fileName']])
            (number_of_rows_cry_file,)=cur.fetchone()

            if number_of_rows_device > 0 and number_of_rows_cry_file > 0 :
                push_service = FCMNotification(api_key=API_KEY_FIREBASE_CLOUD_MESSAGINING)
                proxy_dict = { 
                                "http"  : "http://127.0.0.1",
                            }
                push_service = FCMNotification(api_key=API_KEY_FIREBASE_CLOUD_MESSAGINING, proxy_dict=proxy_dict)
            
                #get the customerID  
                cur.execute("SELECT customer_id FROM device WHERE id LIKE %s",[idDevice])
                result_set = cur.fetchall()
                for row in result_set:
                    idCustomer = row[0]

                cur.execute("SELECT * FROM alert WHERE id LIKE %s",[request.form['alertNumber']])
                result_set = cur.fetchall()
                for row in result_set:
                    idAlert = row[0]
                    titleAlert = row[1]
                    descriptionAlert = row[2]
                
                cur.execute("INSERT INTO customer_alert(id, alertDate, seen, customer_id, alert_id) VALUES (NULL,%s,0,%s,%s)",(datetime.datetime.today().strftime('%Y-%m-%d %H-%M-%S'),idCustomer,idAlert))
                mysql.connection.commit()
                
                #set the status of the soundfile traited by the IA
                cur.execute("UPDATE cry c, device_cry dc SET status = 'traited' WHERE c.id = dc.cry_id AND c.fileName LIKE %s AND dc.device_id LIKE %s",(request.form['fileName'],idDevice))
                mysql.connection.commit()

                #get the Src AudioFile
                cur.execute("SELECT srcAudioFile FROM cry WHERE fileName LIKE %s",[request.form['fileName']])
                result_set = cur.fetchall()
                for row in result_set:
                    srcAudioFile = row[0]

                #set the Information srcAudioFile
                cur.execute("INSERT INTO alert_cry(id, idAlert, srcAudioFile) VALUES (NULL,%s,%s)",(idAlert,srcAudioFile))
                mysql.connection.commit()

                #get the Token Phone
                cur.execute("SELECT token FROM phone WHERE customer_id LIKE %s",[idCustomer])
                result_set = cur.fetchall()
                for row in result_set:
                    tokenDevice = row[0]

                registration_id = tokenDevice
                message_title = titleAlert
                message_body = descriptionAlert+"                                                             ---"+str(idAlert)+";;;com.example.administrator.test.myfFCMActionClick"
                result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title, message_body=message_body)
                return make_response('succes'+str(result),200)
            else:
                return make_response('misspelling idDevice or fileName')
        except Exception as e:
            return make_response('Error in database Connection',506)
        finally:
            if cur is not None:
                cur.close() 
    else:
        return make_response('unknown Request Type',310)

@app.route('/restApi/Hablo_v1/Android/<idCustomer>/alert/<idAlert>')
def setAlertAsSeen(idCustomer,idAlert):
    cur=None
    try:
        cur = mysql.connection.cursor()
        cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
        (number_of_rows,)=cur.fetchone()
        if number_of_rows > 0:
            cur.execute("UPDATE customer_alert SET seen=1 WHERE customer_id=%s AND alert_id = %s",(idCustomer,idAlert));
            mysql.connection.commit()
            cur.execute("SELECT a.id,title,description,alertDate,seen,c.customer_id FROM alert a,customer_alert c WHERE a.id = c.alert_id AND c.customer_id LIKE %s AND seen LIKE 0",[idCustomer])
            row_headers=[x[0] for x in cur.description] #this will extract row headers
            rv = cur.fetchall()
            json_data=[]
            for result in rv:
                json_data.append(dict(zip(row_headers,result)))
            return jsonify({"alerts":json_data})
        else:
            return make_response('unknown Client',309)
    except Exception as e:
        return make_response('Error in database Connection',506)
    finally:
        if cur is not None:
            cur.close() 


@app.route('/restApi/Hablo_v1/hard/login')
def indexLogin():
    if request.authorization :
        #get the login and the password and verify them with our database
        cur = mysql.connection.cursor()
        cur.execute("SELECT count(*) FROM customer WHERE login LIKE %s AND pwd LIKE %s",[request.authorization.username,request.authorization.password])
        (number_of_rows,)=cur.fetchone()
        cur.close()
        if number_of_rows > 0:
            return make_response('successful connection', 200)
        return make_response('Could not verify!', 401, {'WWW-Authenticate' : 'Basic realm="Login Required"'})
    return make_response('Could not verify!', 401, {'WWW-Authenticate' : 'Basic realm="Login Required"'})


@app.route('/restApi/Hablo_v1/hard/<idDevice>/uploadsFile', methods=['POST'])
def uploadFileCryViaPost(idDevice):
    cur = None
    try:
        if request.method == 'POST' :
            cur = mysql.connection.cursor()
            cur.execute("SELECT COUNT(*) from device where id LIKE %s",[idDevice])
            (number_of_rows,)=cur.fetchone()
            if number_of_rows > 0:
                if 'soundFile' in request.files:
                    file = request.files['soundFile']
                    if not os.path.exists(UPLOAD_FOLDER+"/Device/"+idDevice):
                        os.makedirs(UPLOAD_FOLDER+"/Device/"+idDevice)
                    if file and allowed_file(file.filename):
                        filename = secure_filename(file.filename)
                        file.save(os.path.join(app.config['UPLOAD_FOLDER']+"/Device/"+idDevice+"/", filename))    
                        idCry = str(random.randint(1000000,9999999))
                        cur.execute("INSERT INTO cry (id,fileName, srcAudioFile) VALUES (%s, %s, %s);",(idCry,filename,SERVER_ADRESSE+UPLOAD_FOLDER+"/Device/"+idDevice+"/"+filename))
                        cur.execute("INSERT INTO device_cry(device_id, cry_id, cryDate,status) VALUES (%s,%s,%s,'status')",[idDevice,idCry,datetime.datetime.today().strftime('%Y-%m-%d %H-%M-%S')])
                        mysql.connection.commit()
			write_into_file("/var/www/api/"+UPLOAD_FOLDER+"/Device/"+idDevice+"/"+filename)
                        return make_response(file.filename, 201)
                    else:
                        return make_response('file format not authorized', 202)
                else:
                    return make_response('soundFile not found into the Post Request', 203)
            else:
                return make_response('unknown device', 308)
        else:
            return make_response('unknown Request Type',310)
    except Exception as e:
        return make_response('Error in database Connection',506)
    finally:
        if cur is not None:
            cur.close()


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/restApi/Hablo_v1/Android/login', methods=['POST'])
def index():
    if request.method == 'POST' :
        #get the login and the password and verify them with our database
        cur = None
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT COUNT(*) FROM customer c,device d WHERE c.id = d.customer_id AND d.token LIKE %s AND c.pwd LIKE %s",[request.form['login'],request.form['pwd']])
            (number_of_rows,)=cur.fetchone()
            if number_of_rows > 0:
                cur.execute("SELECT * FROM customer WHERE pwd LIKE %s",[request.form['pwd']])
                row_headers=[x[0] for x in cur.description] #this will extract row headers
                rv = cur.fetchall()
                json_data=[]
                for result in rv:
                    json_data.append(dict(zip(row_headers,result)))
                return jsonify(json_data[0])
            return make_response('Could not verify!', 401, {'WWW-Authenticate' : 'Basic realm="Login Required"'})
        except Exception as e:
            return make_response('Error in database Connection',506)
        finally:
            if cur is not None:
                cur.close()
    return make_response('Could not verify!', 401, {'WWW-Authenticate' : 'Basic realm="Login Required"'})

@app.route('/restApi/Hablo_v1/Android/<idCustomer>/Save', methods=['POST'])
def saveBabyInformation(idCustomer): 
    if request.method == 'POST' :
        cur = None
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
            (number_of_rows,)=cur.fetchone()
            if number_of_rows > 0:
                idBaby = str(random.randint(1000000,9999999))
                cur.execute("INSERT INTO baby (id, customer_id, gender, birthDate, inscriDate) VALUES (%s, %s, %s, %s, %s)",[idBaby,idCustomer,request.form['gender'],request.form['birthDate'],datetime.datetime.today().strftime('%Y-%m-%d')])
                mysql.connection.commit()                   
                cur.execute("SELECT * FROM baby WHERE id=%s",[idBaby])
                row_headers=[x[0] for x in cur.description] #this will extract row headers
                rv = cur.fetchall()
                json_data=[]
                for result in rv:
                    json_data.append(dict(zip(row_headers,result)))
                return jsonify(json_data[0])
            else:
                return make_response('unknown Client',309)
        except Exception as e:
            return make_response('Error in database Connection',506)
        finally:
            if cur is not None:
                cur.close()
    else:
        return make_response('unknown Request Type',310) 

@app.route('/restApi/Hablo_v1/Android/<idCustomer>/bebe')
def getBabyList(idCustomer):
    cur = None
    try:
        cur = mysql.connection.cursor()
        cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
        (number_of_rows,)=cur.fetchone()
        if number_of_rows > 0:
            cur.execute("SELECT * FROM baby WHERE customer_id=%s",[idCustomer])
            row_headers=[x[0] for x in cur.description] #this will extract row headers
            rv = cur.fetchall()
            json_data=[]
            for result in rv:
                json_data.append(dict(zip(row_headers,result)))
            return jsonify({"nbBebes":len(json_data),"childs":json_data})
        else:
            return make_response('unknown customer',309)
    except Exception as e:
        return make_response('Error in database Connection',506)
    finally:
        if cur is not None:
            cur.close()

@app.route('/restApi/Hablo_v1/Android/<idCustomer>/alertnotseen')
def getalertListNotSeenYet(idCustomer):
    cur=None
    try:
        cur = mysql.connection.cursor()
        cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
        (number_of_rows,)=cur.fetchone()
        if number_of_rows > 0:
            cur.execute("SELECT a.id,title,description,alertDate,seen,c.customer_id FROM alert a,customer_alert c WHERE a.id = c.alert_id AND c.customer_id LIKE %s AND seen LIKE 0",[idCustomer])
            row_headers=[x[0] for x in cur.description] #this will extract row headers
            rv = cur.fetchall()
            json_data=[]
            for result in rv:
                json_data.append(dict(zip(row_headers,result)))
            return jsonify({"alerts":json_data})
        else:
            return make_response('unknown Client',309)
    except Exception as e:
        return make_response('Error in database Connection',506)
    finally:
        if cur is not None:
            cur.close() 

@app.route('/restApi/Hablo_v1/Android/<idCustomer>/alert')
def getalertList(idCustomer):
    cur=None
    try:
        cur = mysql.connection.cursor()
        cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
        (number_of_rows,)=cur.fetchone()
        if number_of_rows > 0:
            cur.execute("SELECT a.id,title,description,alertDate,seen,c.customer_id FROM alert a,customer_alert c WHERE a.id = c.alert_id AND c.customer_id LIKE %s",[idCustomer])
            row_headers=[x[0] for x in cur.description] #this will extract row headers
            rv = cur.fetchall()
            json_data=[]
            for result in rv:
                json_data.append(dict(zip(row_headers,result)))
            return jsonify({"alerts":json_data})
        else:
            return make_response('unknown Client',309)
    except Exception as e:
        return make_response('Error in database Connection',506)
    finally:
        if cur is not None:
            cur.close() 

@app.route('/restApi/Hablo_v1/Android/<idCustomer>/emergencyForm', methods=['GET', 'POST'])
def getTheInformationFormEmergencyForm(idCustomer):
    cur=None
    try:
        cur = mysql.connection.cursor()
        cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
        (number_of_rows,)=cur.fetchone()
        if number_of_rows > 0:
            if request.method == 'POST' :
                cur = mysql.connection.cursor()
                #cur.execute("UPDATE childs SET nom=%s,prenom=%s WHERE ID_Child=%s",[request.form['nom'],request.form['prenom'],ID_Child])
                #mysql.connection.commit()
                return "information1"
            else:
                return make_response('unknown Request Type',310)
        else:
            return make_response('unknown Client',309)
    except Exception as e:
        return make_response('Error in database Connection',506)
    finally:
        if cur is not None:
            cur.close() 

@app.route('/restApi/Hablo_v1/Android/<idCustomer>/updateinfocustomer', methods=['POST'])
def updateInformationCustomer(idCustomer):
    if request.method == 'POST' :
        cur=None
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
            (number_of_rows,)=cur.fetchone()
            if number_of_rows > 0:
                if 'srcImg' in request.files :
                    altImg = idCustomer+" "+request.form['name']+" "+request.form['lastname']
                    #make the photo upload
                    file = request.files['srcImg']
                    if os.path.exists(UPLOAD_FOLDER+"/customer/"+idCustomer):
                        shutil.rmtree(UPLOAD_FOLDER+"/customer/"+idCustomer)
                        os.makedirs(UPLOAD_FOLDER+"/customer/"+idCustomer)
                    else:
                        os.makedirs(UPLOAD_FOLDER+"/customer/"+idCustomer)
                    fileName = idCustomer+"_"+file.filename
                    if file and allowed_file(file.filename):
                        filename = secure_filename(fileName)
                        file.save(os.path.join(app.config['UPLOAD_FOLDER']+"/customer/"+idCustomer+"/", filename))
                    cur.execute("UPDATE customer SET name=%s,lastname=%s,email=%s,phone=%s,srcImg=%s,altImg=%s WHERE id=%s",[request.form['name'],request.form['lastname'],request.form['email'],request.form['phone'],SERVER_ADRESSE+UPLOAD_FOLDER+"/customer/"+idCustomer+"/"+filename,altImg,idCustomer])
                else:
                    cur.execute("UPDATE customer SET name=%s,lastname=%s,email=%s,phone=%s WHERE id=%s",[request.form['name'],request.form['lastname'],request.form['email'],request.form['phone'],idCustomer])
                mysql.connection.commit()                   
                cur.execute("SELECT * FROM customer WHERE id=%s",[idCustomer])
                row_headers=[x[0] for x in cur.description] #this will extract row headers
                rv = cur.fetchall()
                json_data=[]
                for result in rv:
                    json_data.append(dict(zip(row_headers,result)))
                return jsonify(json_data[0])
            else:
                return make_response('unknown Client',309)
        except Exception as e:
            return make_response('Error in database Connection',506)
        finally:
            if cur is not None:
                cur.close()
    else:
        return make_response('unknown Request Type',310)
        
@app.route('/restApi/Hablo_v1/Android/<idCustomer>/passwordupdate', methods=['POST'])
def updatePasswordCustomer(idCustomer):
    if request.method == 'POST' :
        cur=None
        try:
            if 'oldpwd' in  request.form:
                cur = mysql.connection.cursor()
                cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
                (number_of_rows,)=cur.fetchone()
                if number_of_rows > 0:
                    cur.execute("SELECT COUNT(*) from customer where id LIKE %s AND login LIKE %s AND pwd LIKE %s",[idCustomer,request.form['login'],request.form['oldpwd']])
                    (number_of_rows,)=cur.fetchone()
                    if number_of_rows > 0:
                        cur.execute("UPDATE customer SET pwd=%s WHERE id=%s",[request.form['newpwd'],idCustomer])
                        mysql.connection.commit()
                        cur.execute("SELECT * FROM customer WHERE id=%s",[idCustomer])
                        row_headers=[x[0] for x in cur.description] #this will extract row headers
                        rv = cur.fetchall()
                        json_data=[]
                        for result in rv:
                            json_data.append(dict(zip(row_headers,result)))
                        return jsonify(json_data[0])
                    else:
                        return make_response('wrong oldpwd !! try again',312)
                else:
                    return make_response('unknown Client',309)
            else:
                return make_response("ur POST request must have a fields named(login,oldpwd,newpwd)",320)
        except Exception as e:
            return make_response('Error in database Connection',506)
        finally:
            if cur is not None:
                cur.close() 
    else:
        return make_response('unknown Request Type',310)


@app.route('/restApi/Hablo_v1/Android/<idCustomer>/pictureupdatecustomer', methods=['POST'])
def updatePictureCustomer(idCustomer):
    if request.method == 'POST' :
        cur=None
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
            (number_of_rows,)=cur.fetchone()
            if number_of_rows > 0:
                if 'srcImg' in request.files :
                    #make the photo upload
                    file = request.files['srcImg']
                    if os.path.exists(UPLOAD_FOLDER+"/customer/"+idCustomer):
                        shutil.rmtree(UPLOAD_FOLDER+"/customer/"+idCustomer)
                        os.makedirs(UPLOAD_FOLDER+"/customer/"+idCustomer)
                    else:
                        os.makedirs(UPLOAD_FOLDER+"/customer/"+idCustomer)
                    fileName = idCustomer+"_"+file.filename
                    if file and allowed_file(file.filename):
                        filename = secure_filename(fileName)
                        file.save(os.path.join(app.config['UPLOAD_FOLDER']+"/customer/"+idCustomer+"/", filename))
                    cur.execute("UPDATE customer SET srcImg=%s WHERE id=%s",[SERVER_ADRESSE+UPLOAD_FOLDER+"/customer/"+idCustomer+"/"+filename,idCustomer])
                    mysql.connection.commit()
                    cur.execute("SELECT * FROM customer WHERE id=%s",[idCustomer])
                    row_headers=[x[0] for x in cur.description] #this will extract row headers
                    rv = cur.fetchall()
                    json_data=[]
                    for result in rv:
                        json_data.append(dict(zip(row_headers,result)))
                    return jsonify(json_data[0])
                else:
                    return make_response('Form to update Picture Customer missing field named(srcImg)',313)
            else:
                return make_response('unknown Client',309)
        except Exception as e:
            return make_response('Error in database Connection',506)
        finally:
            if cur is not None:
                cur.close() 
    else:
        return make_response('unknown Request Type',310)

@app.route('/restApi/Hablo_v1/Android/activatedevice', methods=['POST'])
def activateDevice():
    if request.method == 'POST' :
        cur=None
        try:
            if 'token' in request.form : 
                cur = mysql.connection.cursor()
                cur.execute("SELECT COUNT(*) from device where token LIKE %s",[request.form['token']])
                (number_of_rows,)=cur.fetchone()
                if number_of_rows > 0:
                    cur.execute("UPDATE device SET active=%s WHERE  token=%s",['1',request.form['token']])
                    mysql.connection.commit()
                    cur.execute("SELECT * from device where token LIKE %s",[request.form['token']])
                    row_headers=[x[0] for x in cur.description] #this will extract row headers
                    rv = cur.fetchall()
                    json_data=[]
                    for result in rv:
                        json_data.append(dict(zip(row_headers,result)))
                    return jsonify(json_data[0])
                else:
                    return make_response('unknown device token',309)
            else:
                return make_response('write give us the token of ur device',316)
        except Exception as e:
            return make_response('Error in database Connection',506)
        finally:
            if cur is not None:
                cur.close() 
    else:
        return make_response('unknown Request Type',310)

@app.route('/restApi/Hablo_v1/Android/setdevicetoken/<idCustomer>', methods=['POST'])
def setDeviceToken(idCustomer):
    if request.method == 'POST' :
        cur = None
        try:
            cur = mysql.connection.cursor()
            cur.execute("SELECT COUNT(*) from customer where id LIKE %s",[idCustomer])
            (number_of_rows,)=cur.fetchone()
            if number_of_rows > 0:
                cur.execute("SELECT COUNT(*) from phone where customer_id LIKE %s",[idCustomer])
                (number_of_rows,)=cur.fetchone()
                if number_of_rows == 0:
                    cur.execute("INSERT INTO phone (id, token, customer_id) VALUES (NULL, %s, %s)",[request.form['tokenDevice'],idCustomer])
                else:
                    cur.execute("UPDATE phone SET token =%s WHERE customer_id = %s",[request.form['tokenDevice'],idCustomer])
                mysql.connection.commit()
                cur.execute("SELECT * from customer where id LIKE %s",[idCustomer])
                row_headers=[x[0] for x in cur.description] #this will extract row headers
                rv = cur.fetchall()
                json_data=[]
                for result in rv:
                    json_data.append(dict(zip(row_headers,result)))
                return jsonify(json_data[0])

            else:
                return make_response('unknown Client',309)
        except Exception as e:
            return make_response('Error in database Connection',506)
        finally:
            if cur is not None:
                cur.close()
    else:
        return make_response('unknown Request Type',310)

def write_into_file(fileUrl):
	fileName =  "/var/www/api/Logs/"+'iaqeue_'+str(int(time.time()))
	f = open(fileName+".ai","w+")	
	f.write(fileUrl)
	f.close()

if __name__ == '__main__':
    app.secret_key = binascii.hexlify(os.urandom(24))
    app.run(host= '10.68.10.10',port="80")
