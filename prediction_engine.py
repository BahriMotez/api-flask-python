import keras
from keras.models import load_model
from keras.layers import Activation, Dense, Dropout, Conv2D, \
                         Flatten, MaxPooling2D
import numpy as np
import librosa
import librosa.display
import os 

import Queue

import requests
url = 'http://tuleap.odc.tn/restApi/Hablo_v1/IA/pushnotif/1'
payload = {'alertNumber': '1', 'fileName': 'output_1537016119.wav'}




QUEUELOGSFILESPATH= "/var/www/api/Logs/"
 
q = Queue.Queue()

def removeFile(filePath):
    os.remove(filePath)

def putIntoQueue(fileName): 
    q.put(fileName)
 
def getFromQueue():
	if not q.empty():
    		return q.get()
	return "none"

def readCueValue(file_path):
     f= open(file_path,"r+")
     
     if f.mode == 'r': 
          contents =f.read()
          
     fl =f.readlines()
     putIntoQueue(fl[0]+":"+file_path);
     f.close()
	

def Files_path_FIFO(Dir_path):
	for file in os.listdir(Dir_path): 
		if file.endswith(".ai"): 
			readCueValue(Dir_path+file)	

def Predict():
	soudFilePath = getFromQueue();
	if(soudFilePath != "none"):
		out=soudFilePath.split(':')
		y, sr = librosa.load(out[0], duration=2.96)
		ps = librosa.feature.melspectrogram(y=y, sr=sr) 
		predection = []
		print ps.shape
		try :
			X_test =np.array([ps.reshape( (128, 128, 1) )])
			predection = model.predict(X_test, batch_size=128)
		except ValueError:
			print "value error "
		# print the classes, the images belong to
		try:
			removeFile(out[1])
		except:
			print "os error "
			
		# POST with form-encoded data
		try:
			index= np.argmax(max(predection))
			if(index <=4):
				out_audio=out[0].split('_')
				payload = {'alertNumber': index, 'fileName': 'output_'+out_audio[1]}
				r = requests.post(url, data=payload)
				print index 
				print predection 
				print r
			else:
				print index
				print predection
		except:
			print " max" 
def Notication():
	return 0 

# dimensions of our images
img_width, img_height = 320, 240

# load the model we saved
model = load_model('/root/hablo_Brains/my_model.h5')
model.compile(loss='categorical_crossentropy',
              optimizer='Adam',
              metrics=['accuracy'])

while True:
	Files_path_FIFO(QUEUELOGSFILESPATH)
	Predict()
'''
y, sr = librosa.load('code/UrbanSound8K/audio/fold1/102842-3-1-0.wav', duration=2.97)
ps = librosa.feature.melspectrogram(y=y, sr=sr)
'''

